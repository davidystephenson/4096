Meteor.publish('users', function() {
  return Meteor.users.find(
    {},
    { fields: { emails: 1, lists: 1, followers: 1, following: 1, createdAt: 1 } }
  );
});

Meteor.publish('grids', function() {
  return Grids.find();
});
