Template.home.onCreated(function() {
  var self = this;

  self.subscribe('users');
  self.subscribe('grids');
});

Template.home.events({
  'click .new-grid': function(e, template) {
    Meteor.call('createGrid');
  },
});

Template.home.helpers({
  grids: function() {
    var grids = Grids.find({});

    return grids;
  },

  isUser: function() { return Meteor.user(); },

  users: function() {
    var users = Meteor.users.find({ _id: { $ne: Meteor.userId() } });

    return users;
  },

  getUserEmail: function(user) { return user.emails[0].address; },
});
