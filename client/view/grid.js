var move = function(gridId, direction) {
  Meteor.call('move', gridId, direction, function(error, result) {
    if (error) console.log(error);
    else Meteor.call('spawn', gridId);
  });
};

Template.grid.onCreated(function() {
  var self = this;

  self.subscribe('grids');

  self.gridId = Router.current().params._id;
  
  self.grid = function() { return Grids.findOne(self.gridId); };

  var keys = {};
  keys[37] = function() { move(self.gridId, 'left'); }; // Left
  keys[38] = function() { move(self.gridId, 'up'); }; // Up
  keys[39] = function() { move(self.gridId, 'right'); }; // Right
  keys[40] = function() { move(self.gridId, 'down'); }; // Down
  keys[65] = function() { move(self.gridId, 'left'); }; // a
  keys[68] = function() { move(self.gridId, 'right'); }; // d
  keys[72] = function() { move(self.gridId, 'left'); }; // h
  keys[74] = function() { move(self.gridId, 'down'); }; // j
  keys[75] = function() { move(self.gridId, 'up'); }; // k
  keys[76] = function() { move(self.gridId, 'right'); }; // l
  keys[83] = function() { move(self.gridId, 'down'); }; // s
  keys[87] = function() { move(self.gridId, 'up'); }; // w

  $(document).on('keyup', function(e) {
    var tag = e.target.tagName.toLowerCase();
    if (tag != 'input' && tag != 'textarea') {
      console.log('key test:', e.which);
      if (keys[e.which]) keys[e.which]();
    }
  });
});

Template.grid.events({
  'click .down': function (e, template) {
    move(template.gridId, 'down');
  },

  'click .left': function (e, template) {
    move(template.gridId, 'left');
  },

  'click .right': function (e, template) {
    move(template.gridId, 'right');
  },


  'click .up': function (e, template) {
    move(template.gridId, 'up');
  },

  'click .reset': function(e, template) {
    Meteor.call('reset', template.gridId);
  }
});

Template.grid.helpers({
  cell: function(x, y) {
    var self = this;

    var gridId = Template.instance().gridId;

    var grid = Grids.findOne(gridId);

    if (grid && grid.cells) {
      value = grid.cells[x][y];
      
      if (value !== 0) {
        var coordinates = [x, y].map(Number);

        if (_.isEqual(grid.spawnLocation, coordinates)) {
          return '<span class="spawn">' + value + '</span>';
        }
        return value;
      }
    }
  },
});
