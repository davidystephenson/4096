var slide = function(input) {
  console.log('input test:', input);

  // Remove empty elements from the input
  
  input = input.filter(function(element) { return element !== 0; });

  var output = [0, 0, 0, 0];

  var inputPosition = 0;

  var outputPosition = 0;

  while(inputPosition < input.length) {
    if (input[inputPosition] === input[inputPosition + 1]) {
      output[outputPosition] = input[inputPosition] * 2;
      outputPosition ++;
      inputPosition += 2;
    } else {
      output[outputPosition] = input[inputPosition];
      outputPosition ++;
      inputPosition ++;
    }
  }

  console.log('output test:', output);

  return output;
};

console.log(slide([2, 0, 0, 2]));
