Grids = new Mongo.Collection('grids');

var slide = function(input) {
  // Remove empty elements from the input
  input = input.filter(function(element) { return element !== 0; });

  var output = [0, 0, 0, 0];

  var inputPosition = 0;

  var outputPosition = 0;

  while(inputPosition < input.length) {
    if (input[inputPosition] === input[inputPosition + 1]) {
      output[outputPosition] = input[inputPosition] * 2;
      outputPosition ++;
      inputPosition += 2;
    } else {
      output[outputPosition] = input[inputPosition];
      outputPosition ++;
      inputPosition ++;
    }
  }

  return output;
};

var flip = function(cells) {
  for (var i = 0; i < cells.length; i ++) {
    cells[i].reverse();
  }

  return cells;
};

var transpose = function(cells) {
  return cells[0].map(function(col, i) { 
    return cells.map(function(row) { 
      return row[i] ;
    });
  });
};

var left = function(cells) {
  for (var i = 0; i < cells.length; i ++) {
    cells[i] = slide(cells[i]);
  }

  return cells;
};

var right = function(cells) { return flip(left(flip(cells))); };

var up = function(cells) { return transpose(left(transpose(cells))); };

var down = function(cells) { return transpose(right(transpose(cells))); };

var emptyCells = [
  [0, 0, 0, 0],
  [0, 0, 0, 0],
  [0, 0, 0, 0],
  [0, 0, 0, 0]
];

var makeSpawn = function(grid) {
  empties = [];

  for (var i = 0; i < grid.cells.length; i ++) {
    for (var j = 0; j < grid.cells[i].length; j ++) {
      if (grid.cells[i][j] === 0) empties.push([i, j]);
    }
  }
  
  if (empties.length === 0) {
    grid.live = false;
    return grid;
  }

  grid.spawnLocation = _.sample(empties);

  grid.spawnValue = Math.random();
  if (grid.spawnValue > 0.5) grid.spawnValue = 4;
  else grid.spawnValue = 2;
};

var applySpawn = function(grid) {
  if (grid.spawnLocation && grid.spawnValue) {
    grid.cells[grid.spawnLocation[0]][grid.spawnLocation[1]] = grid.spawnValue;
  }

  grid.spawnValue = false;
};

var fullSpawn = function(grid) {
  makeSpawn(grid);
  applySpawn(grid);
};

Meteor.methods({
  createGrid: function() {
    var grid = {
      cells: emptyCells,

      live: true,

      merged: [],

      moved: false,

      spawnLocation: false,

      spawnValue: false,
      
      userId: false,
    };

    fullSpawn(grid);
    fullSpawn(grid);

    Grids.insert(grid);
  },

  move: function(gridId, direction) {
    check(gridId, String);
    check(direction, String);

    var grid = Grids.findOne(gridId);

    var directions = {
      'down': down,
      'left': left,
      'right': right,
      'up': up,
    };

    var oldCells = JSON.parse(JSON.stringify(grid.cells));

    grid.cells = directions[direction](grid.cells);

    if (_.isEqual(oldCells, grid.cells)) console.log('Warning: not a valid move');
    else makeSpawn(grid);

    Grids.update(gridId, grid);
  },

  reset: function(gridId) {
    check(gridId, String);

    var grid = Grids.findOne(gridId);

    grid.cells = emptyCells;

    fullSpawn(grid);
    fullSpawn(grid);

    Grids.update(gridId, grid);
  },

  spawn: function(gridId) {
    check(gridId, String);

    var grid = Grids.findOne(gridId);

    applySpawn(grid);

    Grids.update(gridId, grid);
  },
});
