Router.configure({
  layoutTemplate: 'layout',
  loadingTemplate: 'loading',
});

Router.route('/', { name: 'home' });

Router.route('/ame/:_id', {
  name: 'game',
});

Router.route('/grid/:_id', {
  name: 'grid',
});
